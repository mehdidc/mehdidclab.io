#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Mehdi Cherti'
SITENAME = 'Mehdi Cherti'
SITEURL = ''

STATIC_PATHS = ["img"]

SITELOGO = SITEURL + '/theme/img/profile.png'
FAVICON = SITEURL + '/images/favicon.ico'

PATH = 'content'

TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = 'en'
LOCALE = ("usa",)
# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)
LINKS = []
# Social widget
SOCIAL = []
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

THEME = "themes/Flex"
#
# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

DISQUS_SITENAME = "mehdicherti"
USE_LESS = True
