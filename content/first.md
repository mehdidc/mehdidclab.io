Title: First post
Date: 2017-12-15
Tags: 
category: presentation 
---

My name is Mehdi Cherti, I am a postdoctoral researcher working on machine learning and deep generative models. 
My PhD is about deep generative models and how to use them to study and experiment with novelty generation.
My profile on google scholar is available [here](https://scholar.google.fr/citations?user=JgOyYi8AAAAJ&hl=en&oi=ao).

My other interests on machine learning are hyperparameter optimization and meta-learning.
I am also interested in applying machine learning to natural sciences, such as biology and particle physics.

My interests on computer science in general are program synthesis and computer graphics.

I am also interested into human learning and how to learn more efficiently and increase productivity.

I am also a passionate Python programmer, and I love to contribute to [free softwares](https://www.gnu.org/philosophy/free-sw.en.html).
My public github profile is available here : <https://github.com/mehdidc>.

I was previously at [LAL](https://www.lal.in2p3.fr). Currently, I am at [Mines Paristech](https://www.mines-paristech.fr).
