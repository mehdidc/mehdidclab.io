Title: Improving my productivity using Pomodoro: takeaways after 2 years of practice 
Date: 2017-12-16
Tags:
category: productivity 
---

In this post, I will share my experience on using the [Pomodoro technique](https://en.wikipedia.org/wiki/Pomodoro_Technique) to improve my 
productivity during my PhD.

## Motivation

The first time I heard about this technique is when I did the [Learning to learn course](https://coursera.org/learn/learning-how-to-learn) by Barbara Oakley and Terrence Sejnowski in 2015, which
I encourage everyone interested in self-improvement to do. At that time, I was a very unsatisfied about my productivity and was looking for ways to improve it.
The main reason I was unsatisfied is that I thought I wasted too many time on what I would call distractions, such as reading mails or twitter feeds or news articles or phone calls/notifications.

## Pomodoro

The basic idea is very simple. To do a "Pomodoro", you set up a timer for say 25 minutes, and during that 25 minutes, you focus/work on one and exactly one task. After the 25 minutes finish, you take a 5 minutes break, where you can relax, walk a little bit. Then, you repeat, as many times as you can during the day. After a series of 4 Pomodoros, you can take a longer break (15 or 20 minutes).
To summarize, a series of Pomodoro can be like this:

- 25 minutes of work on one task
- 5 minutes break
- 25 minutes of work on one task
- 5 minutes break
- 25 minutes of work on one task
- 5 minutes break
- 25 minutes of work on one task
- 20 minutes break
- ...

## Planning

Ideally, Pomodoro should not be applied without a dedicated planning. Every day, you need to have a planning of the tasks you need to do. Then, you need to estimate the number of Pomodoros (nb of 25 minutes or so) needed for each task and sort the tasks by priority. The advantage is that it lets you compare the estimated number of Pomodoros and the actual number of Pomodoros per task, which makes you do a better estimation in the future and thus a better planning.

## Finding the App

I started to do Pomodoro seriously in June 2015. Before that, I tried several apps, on smartphone and desktop. However, I could not became used to any of those. 

First, some of these apps need Internet, and I did not  want to be limited by this. Second, some apps did not make it easy to retrieve/use/log the data. Third, I do not use a lot my phone and I wanted an app on my laptop. Finally, I also work a lot in command line, and I wanted something simple on the shell. 

I ended up developing my own simple app based on [this](http://code.activestate.com/recipes/577358-pomodoro-timer/). Check the github repo [https://github.com/mehdidc/pomodoro](https://github.com/mehdidc/pomodoro) if you are interested. It is a really simple and minimal app that makes you set up a timer, then plays a ring when it finishes and logs all the data in a CSV file. This is what ended up working for me for a long period.

Basically, to start a Pomodoro I do this, for instance :

    :::bash
    pomodoro 30 5

It sets up an infinite loop of Pomodoros with 30 minutes of work and 5 minutes of break. To cancel the current Pomodoro, you just have to do CTRL+C. The Pomodoro starting and ending times are recorded on the CSV file once it is finished. I also made a companion app to see graphs, such as the number of hours per day, per week, etc.

As I said, there are many apps out there, and I encourage you to try them, and choose the one that fits you the best, or make your own.

## Pomodoro in practice

The data that I describe here starts in June 2015 and ends in December 2017.
During this period, I tried to use Pomodoro everyday. Sometimes it was difficult to do it, for instance when I have a meeting or I attend a conference.
Moreover, sometimes I work without officially doing a Pomodoro, especially when I work with people.
Thus, the data recorded can be thought of as a "lower bound" of a proxy of productivity.

### Planning is difficult

In practice, planning is a very difficult task, especially (but not only) in research where the tasks are not clearly defined and change everytime.
This is the part which I retrospectively found I had the most difficulty with, and that I am looking forward to improve in the future. 
However, even during days when I had difficulty to plan correctly, Pomodoros were useful and served as a way to prevent me for being distracted by non-productive things. Also, Pomodoros were a great 
way of getting me started working instead of procrastinating.

### Flexibility is important

It is important to not be stuck on the durations that were proposed originally by the author of Pomodoro. 25 minutes of work and 5 minutes of break may not be convenient for everybody.
You may sometimes require 25 minutes, but some other times 1 hour or more.
The best is to find the durations that you are most comfortable with. I ended up using a mix of different durations. Sometimes, I do 15 minutes, or 30 minutes, sometimes one hour, or 2 hours.
Same for the break, sometimes 5 minutes did not work for me.

Below is the graph of the frequency of each duration of the Pomodoros I used.
The most frequent duration was 30 minutes, followed by one hour.

![Frequencies of each duration of the Pomodoros I used]({static}/img/pomodoro/freqs.png)

### Not all days are created equal

As I said before, sometimes it is difficult to do Pomodoro, for different reasons such as admnistrative duties, or attending a conference or working with other people.

However, it is difficult to maintain the same rythm for a long time, even during days where there are no such interruptions. 

Some days I am very motivated and I can do a lot of Pomodoros, some other days I do less. In those unfortunate periods where you are less productive, logging the Pomodoros (or collecting data about yourself, in general) can be used to suggest you that at some point you should switch to another task, or just take a break and refresh yourself.

Below is the total number of hours spent on each day, globally.

![Days distribution]({static}/img/pomodoro/days.png)

My most productive days seem to be Tuesday, Wednesday, and Thursday. Interestingly, the day with the least number of hours is Friday, not Saturday and Sunday.
Notice the pattern, a gradually increase, a peak, and then a gradual decrease suggesting fatigue or boredness.

I noticed a similar pattern on several scales, e.g., on the scale of the weeks or months or even during a single day.

### Most productive hours

As I said, I noticed a pattern with a gradual increase then a peak then a gradual decrease during the days of the week. A similar thing can be observed during a single day.

Below is the total number of hours of completed Pomodoros which have been started within each hour of the day.

![Day]({static}/img/pomodoro/day.png)

In the morning, there is a peak of productivity around 10AM, then it decreases until lunch time. After lunch, it starts to increase with a peak around 4PM, then it starts to decrease from 4PM until
7PM. Then, again, starting from 8PM, I restart working and there is a gradual increase with a peak at 11PM. The reason there is a peak at 11PM is that I often set myself a goal of total number of Pomodoros per day, and I try my best to achieve it during the day. Almost always, I end up not being able to achieve it during the day, and thus I try to do the best to achieve it in the evening.

### Most productive Months

Below is the total number of hours of Pomodoros during each month.

![Months]({static}/img/pomodoro/months.png)

Again, a similar pattern (gradual increase, peak, gradual decrease, and so on) can be observed on the scale of months. October 2016 and August 2017 were the most productive months. Notice the sparseness of the months of August 2015 and August 2016 which correspond to vacations, August 2017 is an exception because I had to start writing my thesis !

### Typical day

Below is a boxplot of the number of hours per day.

![Typical Day]({static}/img/pomodoro/day_avg.png)

The median value is around 6 hours per day. The boxplot shows that 25% of the days I work less than 4 hours and 25% of the days I work more than 8 hours.

## Conclusion

Doing pomodoro for 2 years was a great experience.

The first advantage for me when I started to use Pomodoro is a gain of focus. It was difficult to do in the beginning (and it still is every time I do a have a long break), but then I became used to it.It is very liberating to be able to focus on one thing, and not give in to temptations.

A second advantage is the ability to collect statistics about yourself. I thought this was a very important feature, as it can at least give you a proxy of your daily "productivity", or at least a better proxy than the number of total hours you are at work. It gives you an idea of when you are more (or less) productive. It also helps you set up mini-goals, such as a minimum number of Pomodoros per week or per day. Ideally, when it is possible, you could also use the data to forecast the future to adjust your planning, or serve as a way to motivate yourself to do better than the forecasted number of hours of productivity.

I am looking forward to continue doing it, this time by focusing much more on 
the planning aspect. By recording the tasks as well as the corresponding number of pomodoros that were done to achieve each task/type of task, it will be possible to obtain a much more meaningful proxy of productivity.

I hope that reading this post I will encourage you to give Pomodoro a try.

For those who are interested, the full data is available here : <https://gist.github.com/mehdidc/eb8ee062075fcc0e668411447def394f>.

The simple app that I used to do Pomodoros is available here : <https://github.com/mehdidc/pomodoro>.


UPDATE: [HackerNews discussion](https://news.ycombinator.com/item?id=15956247)
